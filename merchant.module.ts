import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MerchantComponent } from './merchant.component';

@NgModule({
  declarations: [MerchantComponent],
  imports: [
    CommonModule
  ]
})
export class MerchantModule { }
